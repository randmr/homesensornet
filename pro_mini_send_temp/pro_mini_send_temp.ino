#include <LowPower.h>

#include <RFM69.h>
#include <SPI.h>
#include <OneWire.h>

#define NODEID      2
#define NETWORKID   100
#define GATEWAYID   1
#define FREQUENCY   RF69_433MHZ //Match this with the version of your Moteino! (others: RF69_433MHZ, RF69_868MHZ)
#define KEY         "thisIsEncryptKey" //has to be same 16 characters/bytes on all nodes, not more not less!
#define SERIAL_BAUD 115200

OneWire  ds(5);  // on pin 5

boolean requestACK = true;
RFM69 radio;

int sleepMultiplier = 70; // sleepMultiplier * 8 = sleep time in seconds
int sleepTime = 68;  // This increments in a loop to sleep more than 8 seconds (start high to transmit data early, then sleep)

typedef struct {
  int           nodeId; //store this nodeId
  unsigned long uptime; //uptime in ms
  float         temp;   //temperature maybe?
  float         batt;   //battery level
} Payload;
Payload theData;

//battery monitor function
long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}
//end battery monitor function

void setup() {
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  //radio.setHighPower(); //uncomment only for RFM69HW!
  radio.encrypt(KEY);
  char buff[50];
  sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);
}

void loop() {
  // Watchdog timer to conserve battery
  while (sleepTime < sleepMultiplier) {
    sleepTime++;
    Serial.print("sleepTime = ");
    Serial.println(sleepTime);
    Serial.flush();

    while (!(UCSR0A & (1 << UDRE0)))  // Wait for empty transmit buffer
      UCSR0A |= 1 << TXC0;  // mark transmission not complete
    while (!(UCSR0A & (1 << TXC0)));   // Wait for the transmission to complete

    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    return;
  } 
  sleepTime = 0;
  Serial.println("Sleepy time is over; sleepTime = 0");
  
  //get battery level
  float batt = readVcc();
  Serial.print("battery level: ");
  Serial.println(batt);

  // Start of one-wire code
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
  
  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
    return;
  }
  
  if (OneWire::crc8(addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return;
  }
 
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }

  // convert the data to actual temperature
  unsigned int raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // count remain gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    if (cfg == 0x00) raw = raw << 3;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw << 2; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw << 1; // 11 bit res, 375 ms
    // default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  Serial.print("Temperature_F = ");
  Serial.println(fahrenheit);
  // End of one-wire code
  
  //fill in the struct with new values
  theData.nodeId = NODEID;
  theData.uptime = millis();
  theData.temp = fahrenheit;
  theData.batt = batt;

  Serial.print("Node ID: ");
  Serial.println(theData.nodeId);
  Serial.print("uptime: ");
  Serial.println(theData.uptime);
  Serial.print("Temp: ");
  Serial.println(theData.temp);
  Serial.print("Batt: ");
  Serial.println(theData.batt);

  Serial.print("Sending struct (");
  Serial.print(sizeof(theData));
  Serial.print(" bytes) ... ");
  //if (radio.sendWithRetry(GATEWAYID, (const void*)(&theData), sizeof(theData), 0, 100)) {
  //  Serial.print(" ok!");
  //} else {
  //  Serial.print(" nothing...");
  //}
  radio.send(GATEWAYID, (const void*)(&theData), sizeof(theData));
  Serial.println();
}
