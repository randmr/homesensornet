#!/usr/bin/python

import serial
import datetime

temp_log = '/home/pi/myScripts/tempLog/arduinoSensorLog.csv'
device   = '/dev/ttyACM0'
baud     = 115200

ser = serial.Serial(device, baud)
while 1 :
  input = ser.readline()
  date_log = str(datetime.datetime.now())
  string = date_log + " " + input
  print(string)
  with open(temp_log, 'a') as f:
    f.write(string)
    f.flush()
