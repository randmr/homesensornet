#!/usr/bin/python

import os
import fnmatch
import glob
import datetime
import time
import requests

# Get temperature from Weather Underground
r = requests.get('http://api.wunderground.com/api/<api_key>/geolookup/conditions/q/95747.json')
data = r.json()
outsideTemp = data['current_observation']['temp_f']

nodeId = '0'

#Set the Log Path
temp_log = '/var/log/homeSensors/homeSensorLog.csv' 
date_log = str(datetime.datetime.now())

string = date_log 
string += ' nodeId='
string += nodeId
string += " temperature="
string += str(outsideTemp)
string += "\n"

#print string

with open(temp_log, 'a') as f:
	f.write(string)
