#!/usr/bin/python

import os
import fnmatch
import glob
import datetime
import time

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

rootdir = '/sys/bus/w1/devices/'

sensors = {}
sensors['28-00044a385dff'] = ''
sensors['28-00044a3d01ff'] = ''
sensors['28-00044a3c05ff'] = 'officeTemp'
nodeId = '1'

#Change to your device path(s)
lst = []
for subdir, dirs, files in os.walk(rootdir):
    for dir in fnmatch.filter(dirs, "28-*"):
        tmpdir = os.path.join(subdir, dir)
        lst.append(os.path.join(tmpdir, 'w1_slave'))

#Set the Log Path
temp_log = '/var/log/homeSensors/homeSensorLog.csv' 
date_log = str(datetime.datetime.now())

def get_temp(device):
	#To read the sensor data, just open the w1_slave file
	f = open(device, 'r')
	data = f.readlines()
	f.close()
	deg_f = ''
	if data[0].strip()[-3:] == 'YES':
		temp = data[1][data[1].find('t=')+2:]
		#If temp is 0 or not numeric an exception 
		#will occur so lets handle it gracefully
		try:
			if float(temp)==0:
				deg_f = 32
			else:
				deg_f = (float(temp)/1000)*9/5+32
		except:
			print "Error with t=", temp
			pass
	return deg_f

string = date_log 

for device in lst:
	device_name = sensors[device.split('/')[5]]
	temp = str(get_temp(device))
	string += ' nodeId='
        string += nodeId
	string += " temperature="
	string += temp
	# When there are multiple devices, a short pause 
	# interval between reading sensors seems to work best
	time.sleep(1)

string += "\n"

#print string

with open(temp_log, 'a') as f:
	f.write(string)
