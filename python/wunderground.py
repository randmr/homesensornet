#!/usr/bin/python
import requests
import datetime

# set location to output data and get current date/time
temp_log = '/var/log/homeSensors/homeWeatherLog.csv'
date_log = str(datetime.datetime.now())

location = '95747'

r = requests.get('http://api.wunderground.com/api/<api_key>/geolookup/conditions/q/95747.json')
data = r.json()
temp_f = data['current_observation']['temp_f']
weather = data['current_observation']['weather'].strip()
precip_1hr_in = data['current_observation']['precip_1hr_in'].strip()
precip_today_in = data['current_observation']['precip_today_in'].strip()
precip_1hr_mm = data['current_observation']['precip_1hr_metric'].strip()
precip_today_mm = data['current_observation']['precip_today_metric'].strip()

# Log line template: homeWeather: <date/time>, outside=<temp>, currentWeather=<conditions>, precip_1hr=<precip inches and millimeters, precip_today=<precip inches and millimeters>
string = date_log
string += ', location='
string += location
string += ', outside='
string += str(temp_f)
string += ', currentWeather='
string += weather
string += ', precip_1hr_in='
string += precip_1hr_in
string += ', precip_today_in='
string += precip_today_in
string += ', precip_1hr_mm='
string += precip_1hr_mm
string += ', precip_today_mm='
string += precip_today_mm
string += "\n"

#print string

with open(temp_log, 'a') as f:
        f.write(string)
