# README #

### Arduino Sketches ###

* Arduino sketches are for a gateway and multiple remote nodes.  
* Remote nodes are anticipated to operate on battery power and should conserve power if possible
* Remote nodes each have 1 One-Wire temperature sensor connected
* All nodes (remote and gateway) have an RFM69W RF module connected to transmit/receive data
* Gateway sketch is connected to a PC via serial USB to capture output.
** PC is a Raspberry Pi in my configuration.

### Python Scripts ###

* Python Scripts run on Raspberry pi to log data to a log file
* gateway-temp.py pulls temperature data from a directly attached One-Wire temp sensor
* outside-temp.py pulls temperature data from wunderground.com via the free API
* read-serial.py runs all the time to capture serial input from the Arduino gateway
* wunderground.py pulls precipitation data from wunderground.com (not related to the other parts of this project, but I wanted to pull the data and learn to use the API)

### Processing Data ###

* Currently sending data from RPi to a Splunk server via rsyslog.
* Building a dashboard in Splunk to show history/trending data.
* May try to make the dashboard into an app that can be distributed via Splunk.